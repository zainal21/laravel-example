<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BarangController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

/**
 * id
 * nama_barang
 * stok
 * harga
 *
*/

// route -> controller -> (jika ada query) model -> view
Route::get('/', function () {
    return view('welcome');
});

// barang
Route::get('/barang', [BarangController::class, 'index']);
